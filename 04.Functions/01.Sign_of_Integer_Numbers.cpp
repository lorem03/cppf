#include <iostream>
void num() {
  int num = 0;
  std::cin >> num;
  if (num < 0) {
    std::cout << "The number " << num << " is negative.\n";
  } else if (num > 0) {
    std::cout << "The number " << num << " is positive.\n";
  } else {
    std::cout << "The number " << num << " is zero.\n";
  }
}
int main() {
  num();
  return 0;
}