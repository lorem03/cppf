#include <iostream>
int smallestOfThreeNums(int num1, int num2, int num3) {
    int smin = INT32_MAX;
    if (num1 < smin) {
        smin = num1;
    }
    if (num2 < smin) {
        smin = num2;
    }
    if (num3 < smin) {
        smin = num3;
    }
    return smin;
}
int main() {
    int numIn1 = 0;
    int numIn2 = 0;
    int numIn3 = 0;
    std::cin >> numIn1 >> numIn2 >> numIn3;
    std::cout << smallestOfThreeNums(numIn1, numIn2, numIn3) << "\n";
    return 0;
}