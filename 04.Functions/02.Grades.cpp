#include <iostream>

void grades() {
  float grade = 0;
  std::cin >> grade;
  if (grade <= 2.99) {
    std::cout << "Fail\n";
  } else if (grade >= 3.00 && grade <= 3.49) {
    std::cout << "Poor\n";
  } else if (grade >= 3.50 && grade <= 4.49) {
    std::cout << "Good\n";
  } else if (grade >= 4.50 && grade <= 5.49) {
    std::cout << "Very good\n";
  } else {
    std::cout << "Excellent\n";
  }
}
int main() {
  grades();
  return 0;
}