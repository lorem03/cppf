#include <iostream>
void triangle(int num) {
  for (int i = 1; i <= num; i++) {
    for (int j = 1; j <= i; j++) {
      std::cout << j << " ";
    }
    std::cout << "\n";
  }
  for (int i = num - 1; i >= 1; i--) {
    for (int j = 1; j <= i; j++) {
      std::cout << j << " ";
    }
    std::cout << "\n";
  }
}
int main() {
  int numIn = 0;
  std::cin >> numIn;
  triangle(numIn);
  return 0;
}