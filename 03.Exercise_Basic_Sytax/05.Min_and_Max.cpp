#include <cstdint>
#include <iostream>
using namespace std;
int main() {
  int counter = 0;
  int min = INT32_MAX;
  int max = INT32_MIN;
  cin >> counter;
  for (int i = 1; i <= counter; i++) {
    int num;
    cin >> num;
    if (num >= max) {
        max = num;
    } 
    if (num <= min) {
        min = num;
    }
  }
  cout << min << ' ' << max << "\n";
  return 0;
}