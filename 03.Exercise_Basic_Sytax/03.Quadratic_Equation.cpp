#include <iostream>
#include <cmath>
using namespace std;
int main() {
    float a = 0.0f;
    float b = 0.0f;
    float c = 0.0f;
    cin >> a >> b >> c;
    const float epsilon = 0.00001f;
    const float discriminant = b * b - (4 * a * c);
    if (discriminant > 0) {
        const float squareDiscr = sqrt(discriminant);
        const float x1 = (-b + squareDiscr) / (2 * a);
        const float x2 = (-b - squareDiscr) / (2 * a);
        cout << x1 << ' ' << x2 << "\n";

    } else if (fabs(sqrt(discriminant)) < epsilon) {
        const float x1 = -b / (2 * a);
        cout << x1 << "\n";
    } else {
        cout << "no roots\n"; 
    }
    return 0;
}