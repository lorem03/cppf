#include <iostream>
using namespace std;
float hypotenuse(float x, float y) {
  float c = (x * x) + (y * y);
  return c;
}
int main() {
  float x1 = 0.f;
  float y1 = 0.f;
  float x2 = 0.f;
  float y2 = 0.f;
  cin >> x1 >> y1 >> x2 >> y2;
  if (hypotenuse(x2, y2) <= hypotenuse(x1, y1)) {
    cout << '('<<x2 << ", " << y2 << ')' << "\n";
  } else {
    cout << '('<<x1 << ", " << y1 << ')' << "\n";
  }
  return 0;
}
