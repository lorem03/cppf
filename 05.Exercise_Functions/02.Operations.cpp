#include <iostream>
using namespace std;
int addition(int a, int b) {
    return (a + b);
}
int subtraction(int a, int b) {
    return (a - b);
}
int multiplication(int a, int b) {
    return (a * b);
}
int division(int a, int b) {
    return (a / b);
}
int main() {
    int a, b;
    char operatorr;
    cin >> a >> b >> operatorr;
    if (operatorr == '+') {
        cout << addition(a, b) << "\n";
    } else if (operatorr == '-') {
        cout << subtraction(a, b) << "\n";
    } else if (operatorr == '*') {
        cout << multiplication(a, b) << "\n";
    } else {
        if (b == 0) {
            cout << "Can't divide by zero.\n";
        } else {
            cout << division(a, b) << "\n";
        }
    }
    return 0;
}